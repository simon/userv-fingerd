#!/bin/sh

# An alternative implementation of the fingerd userv service, which
# just prints out all its userv-provided information for diagnostic
# purposes.
echo "Testing userv.fingerd."
echo "Socket: $USERV_U_SOCKADDR $USERV_U_SOCKPORT"
echo "Peer: $USERV_U_PEERADDR $USERV_U_PEERPORT"
echo "User details: $USERV_U_USERNAME $USERV_U_SUFFIX"
