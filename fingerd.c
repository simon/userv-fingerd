/*
 * userv-based finger server
 * Copyright (C) 2010 Richard Kettlewell and Simon Tatham
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <config.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pwd.h>

/* We expect to be run from inetd and to have the connection to the
 * client on stdin/stdout.  The client is either trying to be
 * compliant with RFC1288, or alternatively up to no good. 
 *
 * Highlights of the RFC and how we interpret it here
 * ==================================================
 *
 * Data MUST be ASCII (s2.2) and lines end CRLF.  There's some vague
 * wording about octets 128-255 but not enough information for anyone
 * to interpret them correctly.  We deal with this ambiguity by not
 * allowing non-ASCII characters in the input, but not attempting to
 * sanitize user-provided output (plan/project) in any way - that is
 * the client's responsibility.
 *
 * s2.3 gives the grammar for the input in an opaque style and an
 * undocumented format; it is also wrong.  I reproduce my
 * corrected interpretation of it here:
 *
 *    query = "/W" spaces_opt CR LF
 *          | w_opt username CR LF
 *          | w_opt hosts CR LF
 *          | w_opt username hosts CR LF
 *
 *    spaces_opt = {SPACE}
 *
 *    w_opt = "/W" spaces
 *
 *    spaces = SPACE {SPACE}
 *
 *    hosts = "@" hostname {"@" hostname}
 *
 *    username = <not documented>
 *
 *    hostname = <not documented>
 *
 * CR means octet 13; LF means octet 10; SPACE means octet 32.
 *
 * {} means the contents are optional and may be repeated; | means "or".
 *
 * The corrections I've added is to support "username CR LF", without
 * an initial /W, and to allow "/W spaces CR LF".  These are
 * prohibited by the RFC1288 grammar, but it's what the client
 * actually sends.
 *
 * Hostname syntax isn't documented.  However, this implementation
 * does not support forwarding, so the point is moot: all queries with
 * a "hosts" part are rejected.
 *
 * Username syntax isn't documented either.  This finger daemon
 * accepts ASCII letters, digits, and the '+' character (and treats
 * the latter specially, separating the actual username from a
 * user-controlled suffix).
 */

#define FINGER_PSEUDO_USERS_FILE "/etc/fingerusers"

static int isuserchar(int c) {
  if(c >= 'a' && c <= 'z')
    return 1;
  if(c >= 'A' && c <= 'Z')
    return 1;
  if(c >= '0' && c <= '9')
    return 1;
  if(c == '+')
    return 1;
  return 0;
}
static FILE *popen_argv_r(char **argv) {
  pid_t pid;
  int pipefd[2];

  if (pipe(pipefd) < 0)
    return NULL;

  pid = fork();
  if (pid < 0) {
    return NULL;
  } else if (pid == 0) {
    dup2(pipefd[1], 1);
    close(pipefd[1]);
    close(pipefd[0]);
    execvp(argv[0], argv);
    _exit(127);
  } else {
    close(pipefd[1]);
    return fdopen(pipefd[0], "r");
  }
}
#define max(a,b) ((a)>(b)?(a):(b))
static const char *addr_to_text(struct sockaddr_storage *addr, int *port)
{
  char *ret;
  /*
   * Why oh why can't inet_ntop or some relative of its just take
   * the struct sockaddr as an argument, so I don't have to switch
   * on address families myself to decide how to extract the
   * relevant information?
   */
  switch (addr->ss_family) {
   case AF_INET:
    *port = ntohs(((struct sockaddr_in *)addr)->sin_port);
    ret = malloc(INET_ADDRSTRLEN);
    if (ret && inet_ntop(AF_INET, &((struct sockaddr_in *)addr)->sin_addr,
			 ret, INET_ADDRSTRLEN))
      return ret;
    else
      return "unavailable";
   case AF_INET6:
    *port = ntohs(((struct sockaddr_in6 *)addr)->sin6_port);
    ret = malloc(INET6_ADDRSTRLEN);
    if (ret && inet_ntop(AF_INET6, &((struct sockaddr_in6 *)addr)->sin6_addr,
			 ret, INET6_ADDRSTRLEN))
      return ret;
    else
      return "unavailable";
   default:
    return "unavailable";
  }
}
int main(void) {
  char input[1024], user[sizeof input] = { 0 };
  const char *s;
  int w = 0;

  /* We don't want to be root. */
  assert(getuid());
  assert(geteuid());
  /* Read the query */
  if(fgets(input, sizeof input, stdin) == NULL) {
    if(ferror(stdin))
      perror("stdin");
    return -1;
  }
  /* Sanity check */
  assert(memchr(input, 0, sizeof input) != NULL);
  s = input;
  /* Queries may start with a /W */
  if(s[0] == '/' && s[1] == 'W') {
    w = 1;
    s += 2;
    /* /W must be followed by spaces or CRLF */
    if(*s == ' ') {
      while(*s == ' ')
        ++s;
    } else if(s[0] == '\r' && s[1] == '\n')
      ;
    else {
      fputs("Malformed query.\n", stderr);
      return -1;
    }
  }
  /* Queries may contain a username */
  if(isuserchar((unsigned char)*s)) {
    char *u = user;
    while(isuserchar((unsigned char)*s))
      *u++ = *s++;
    *u = 0;
    /* user[] is the same size as input[], so there is no chance of
     * overflow. */
  }
  /* Queries might contain a @hostname sequence, which we would check
   * here, but this implementation does not support forwarding,
   * so we don't. */
  if(s[0] == '@') {
    fputs("Forwarding is not supported, sorry.\r\n", stderr);
    return 0;
  }
  /* We'd better have reached the end of the line. */
  if(!(s[0] == '\r' && s[1] == '\n')) {
    fputs("Malformed query.\n", stderr);
    return -1;
  }
  /* For now we don't support enumeration of logged-in users.  This
   * could be supported but still seems like a privacy violation. */
  if(!*user) {
    fputs("User enumeration is not supported, sorry.\n", stderr);
    return -1;
  }
  char *suffix = user + strcspn(user, "+");
  if (*suffix)
    *suffix++ = '\0';
  const struct passwd *pw = getpwnam(user);
  if(!pw) {
    FILE *fp = fopen(FINGER_PSEUDO_USERS_FILE, "r");
    if (fp) {
      char confline[4096];

      while (fgets(confline, sizeof(confline), fp)) {
	/*
	 * Read lines from the config file of the form
	 * 'pseudouser:realuser'. If the username we've been given
	 * matches the first field, use the second as our lookup in
	 * the password file.
	 */
	confline[strcspn(confline, "\n")] = '\0';
	if (confline[0] == '#')
	  continue;		       /* skip comment line */
	char *realuser = strchr(confline, ':');
	if (!realuser)
	  continue;		       /* skip malformed line */
	*realuser++ = '\0';
	if (strcmp(user, confline))
	  continue;		       /* skip non-matching line */
	pw = getpwnam(realuser);
	break;			       /* found it! */
      }
      fclose(fp);
    }
  }
  if (!pw) {
    printf("No such user.\r\n");
    return 0;
  }
  /*
   * Get the socket and peer addresses. We'll pass these into userv
   * via -D, so that the service can use them to decide what or how
   * much output to print.
   */
  struct sockaddr_storage peername, sockname;
  socklen_t peerlen = sizeof(peername), socklen = sizeof(sockname);
  const char *peerstr, *sockstr;
  int peerport = 0, sockport = 0;
  if (getpeername(0, (struct sockaddr *)&peername, &peerlen) < 0) {
    peerstr = "unavailable";
  } else {
    peerstr = addr_to_text(&peername, &peerport);
  }
  if (getsockname(0, (struct sockaddr *)&sockname, &socklen) < 0) {
    sockstr = "unavailable";
  } else {
    sockstr = addr_to_text(&sockname, &sockport);
  }
  /* We print the username ourselves, just as a confirmation of what fingerd
   * understood the client to be saying, so that the userv service can't be
   * _too_ needlessly confusing */
  printf("Username: %s\r\n", user);
  char *command[16];
  char userv[6], service[8], dashD[3];
  int peerdeflen = 10 + strlen(peerstr), sockdeflen = 10 + strlen(sockstr);
  char peerdef[peerdeflen];
  char sockdef[sockdeflen];
  char peerportdef[20], sockportdef[20];
  int suffixdeflen = 8 + strlen(suffix);
  char suffixdef[suffixdeflen];
  int userdeflen = 10 + strlen(user);
  char userdef[userdeflen];
  snprintf(userv, sizeof(userv), "userv");
  snprintf(service, sizeof(service), "fingerd");
  snprintf(dashD, sizeof(dashD), "-D");
  snprintf(peerdef, peerdeflen, "PEERADDR=%s", peerstr);
  snprintf(sockdef, sockdeflen, "SOCKADDR=%s", sockstr);
  snprintf(peerportdef, sizeof(peerportdef), "PEERPORT=%d", peerport);
  snprintf(sockportdef, sizeof(sockportdef), "SOCKPORT=%d", sockport);
  snprintf(userdef, userdeflen, "USERNAME=%s", user);
  snprintf(suffixdef, suffixdeflen, "SUFFIX=%s", suffix);
  command[0] = userv;
  command[1] = dashD;
  command[2] = peerdef;
  command[3] = dashD;
  command[4] = sockdef;
  command[5] = dashD;
  command[6] = peerportdef;
  command[7] = dashD;
  command[8] = sockportdef;
  command[9] = dashD;
  command[10] = suffixdef;
  command[11] = dashD;
  command[12] = userdef;
  command[13] = pw->pw_name;
  command[14] = service;
  command[15] = NULL;
  FILE *pipefp = popen_argv_r(command);
  if (pipefp) {
    /* Transmit the data, translating to wire format */
    int last = -1, c;
    while((c = getc(pipefp)) != EOF) {
      if(c == '\n' && last != '\r')
	putchar('\r');
      putchar(c);
      last = c;
    }
    /* Make sure there is a final CR LF */
    if(last != '\n')
      fputs("\r\n", stdout);
  }
  fclose(pipefp);
  return 0;
}

/*
Local Variables:
c-basic-offset:2
comment-column:40
fill-column:79
indent-tabs-mode:nil
End:
*/
