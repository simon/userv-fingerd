/*
 * default finger service for userv-based fingerd
 * Copyright (C) 2010 Richard Kettlewell and Simon Tatham
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <config.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

static const char *make_full_path(const char *dir, const char *file) {
  char *buffer = malloc(strlen(dir) + strlen(file) + 2);
  if(!buffer) {
    fputs("Out of memory\n", stderr);
    exit(-1);
  }
  strcpy(buffer, dir);
  strcat(buffer, "/");
  strcat(buffer, file);
  return buffer;
}

static void show_file(const struct passwd *pw,
                      const char *heading,
                      const char *file) {
  const char *full_path = make_full_path(pw->pw_dir, file);
  FILE *fp = fopen(full_path, "r");
  if(!fp)
    return;
  printf("%s:\n", heading);
  /* Transmit the file */
  int last = -1, c;
  while((c = getc(fp)) != EOF) {
    putchar(c);
    last = c;
  }
  /* Make sure there is a final CR LF */
  if(last != '\n')
    putchar('\n');
  fclose(fp);
}

int main(void) {
  const struct passwd *pw = getpwuid(getuid());
  if(!pw) {
    printf("No such user.\n");
    return 0;
  }
  /* Basic information about the user */
  const char *end = strchr(pw->pw_gecos, ',');
  if(!end)
    end = pw->pw_gecos + strlen(pw->pw_gecos);
  printf("Name:     %.*s\n", (int)(end - pw->pw_gecos), pw->pw_gecos);
  /* Information the user wants to publish */
  show_file(pw, "Project", ".project");
  show_file(pw, "Plan", ".plan");
  return 0;
}

/*
Local Variables:
c-basic-offset:2
comment-column:40
fill-column:79
indent-tabs-mode:nil
End:
*/
